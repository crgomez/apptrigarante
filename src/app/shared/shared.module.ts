import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular";

import { LoaderComponent } from "./components/loader/loader.component";
import { PrivacityComponent } from "./components/privacity/privacity.component";
import { TabsComponent } from "./components/tabs/tabs.component";

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [
        LoaderComponent,
        TabsComponent,
        PrivacityComponent
    ],
    declarations: [
        LoaderComponent,
        TabsComponent,
        PrivacityComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule { }
