import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";
import { ApplicationSettings, EventData, Page, Switch } from "tns-core-modules";
import { SqliteMethodsClass } from "../../../core/classes/SqliteMethods";


@Component({
    selector: "app-privacity",
    templateUrl: "privacity.component.html",
    styleUrls: ["privacity.component.scss"]
})

export class PrivacityComponent implements OnInit {

    textPrivacity: string = `He leído y acepto el "Aviso de Privacidad"`;

    privacityChecked: boolean = false;

    @Output() privacityAccept = new EventEmitter<boolean>();

    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = true;

    }

    ngOnInit() { }

    onLoad() {
        console.log("Pdf cargado");
    }

    /* Metodo que detecta el cambio de estado y se le asigna a la viarable para validar el estado del botón
    y retornarlo con el EventEmmiter */
    onCheckedChange(evt: EventData) {
        const sw = evt.object as Switch;
        this.privacityChecked = sw.checked;
    }

    acceptPrivacityAndCotinue() {
        /* Cuando se da clic en el botón para aceptar se cambiará el estado de la variable a true indicando que el aviso
        de privacidad ha sido aceptado por el cliente y emitirá un evento para cerrar el componente en ese momento*/
        ApplicationSettings.setBoolean("privacitiy", true);
        this.privacityAccept.emit(this.privacityChecked);
    }

}
