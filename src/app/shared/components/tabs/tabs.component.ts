import { Component, Input, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";

@Component({
    selector: "app-tabs",
    templateUrl: "tabs.component.html",
    styleUrls: ["tabs.component.scss"]
})

export class TabsComponent implements OnInit {

    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = false;
    }

    ngOnInit() { }

    /* getPdf() {
        setTimeout(() => {
            this.pdfurl = "~/assets/aviso/aviso.pdf";
        }, 1000);
    }

    onLoad() { } */
}