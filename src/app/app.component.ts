import { Component, OnInit } from "@angular/core";
import { SqliteMethodsClass } from "~/app/core/classes/SqliteMethods";

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {

    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.sqliteMethods.createTablesSqlite();
    }
}
