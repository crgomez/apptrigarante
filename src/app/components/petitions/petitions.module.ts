import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { PetitionsComponent } from "./components/petitions/petitions.component";
import { PetitionsRoutingModule } from "./petitions-routing.module";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        PetitionsRoutingModule
    ],
    exports: [],
    declarations: [
        PetitionsComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PetitionsModule { }
