import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "app-petitions",
    templateUrl: "petitions.component.html",
    styleUrls: ["petitions.component.scss"]
})

export class PetitionsComponent implements OnInit {

    constructor(
        private routerExtension: RouterExtensions
    ) { }

    ngOnInit() { }

    goBack() {
        this.routerExtension.backToPreviousPage();
    }
}