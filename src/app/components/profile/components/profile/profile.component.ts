import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";
import { ApplicationSettings } from "@nativescript/core";
import { SqliteMethodsClass } from "~/app/core/classes/SqliteMethods";

@Component({
    selector: "app-profile",
    templateUrl: "profile.component.html",
    styleUrls: ["profile.component.scss"]
})

export class ProfileComponent implements OnInit {

    clientData: Array<[]>;
    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();
    userActive: number;

    constructor(
        private routerExtensions: RouterExtensions
    ) {
        this.userActive = ApplicationSettings.getNumber("userActive");
    }

    ngOnInit() {
        this.loadClientData();
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    async loadClientData() {
        const fields = `*`;
        const tableName = `CLIENTES`;
        const clausule = `WHERE ID = ${this.userActive}`;

        await this.sqliteMethods.getData(fields, tableName, clausule)
            .then((result) => {
                this.clientData = result[0];
                console.log("Result =>", this.clientData);
            })
            .catch((error) => {
                console.log("Error => ", error);
            });
    }
}