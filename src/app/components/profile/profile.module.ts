import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { ProfileComponent } from "./components/profile/profile.component";

import { ProfileRoutingModule } from "./profile-routing.module";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ProfileRoutingModule
    ],
    exports: [],
    declarations: [
        ProfileComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class ProfileModule { }
