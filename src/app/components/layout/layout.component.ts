import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";

@Component({
    selector: "app-layout",
    templateUrl: "layout.component.html"
})

export class LayoutComponent implements OnInit {

    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void { }
}