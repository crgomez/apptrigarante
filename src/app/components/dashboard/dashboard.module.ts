import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { SharedModule } from "~/app/shared/shared.module";

import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { DashboardRoutingdModule } from "./dashboard-routing.module";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        DashboardRoutingdModule,
        NativeScriptUISideDrawerModule,
        SharedModule
    ],
    exports: [],
    declarations: [DashboardComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class DashboardModule { }
