import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";
import { isAndroid, isIOS, Page } from "tns-core-modules/ui/page";
import { alert, confirm } from "tns-core-modules/ui/dialogs";
import { AuthService } from "~/app/core/services/auth/auth.service";
import { SqliteMethodsClass } from "~/app/core/classes/SqliteMethods";
import { ApplicationSettings } from "tns-core-modules";
import { DataService } from "~/app/core/services/data/data.service";
import { ClientModel } from "~/app/core/models/Client/ClientModel";
import { DashboardClass } from "~/app/core/classes/DashboardClass";
import { RouterExtensions } from "@nativescript/angular";
import { SideDrawerLocation } from "nativescript-ui-sidedrawer";

@Component({
    selector: "app-dashboard",
    templateUrl: "dashboard.component.html",
    styleUrls: ["dashboard.component.scss"]
})

export class DashboardComponent implements OnInit {

    //#region variables

    fullClientName: string = "";
    // ViewChild donde se mostrará el SideDrawer (menú lateral izquierdo)
    @ViewChild(RadSideDrawerComponent, { static: false })
    drawerComponent: RadSideDrawerComponent;
    /* Se almacena el estado del sidedraer para cuando se vuelva
    a presionar el mismo botón se cierre o se abra dependiendo del estado. */
    isOpenSidedrawer = false;
    // Indica con true o false si el dispositivo es android o ios
    isAndroid: boolean;
    isIOS: boolean;

    isLoading: boolean = false;
    textLoader: string = "";

    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();
    userActive: number;

    // Variable para mostar o no el aviso de privacidad
    activatePrivacity: boolean = false;

    // Variabe para ejecutar la clase que actualizará los datos del usuario actual
    dashboardClass: DashboardClass = new DashboardClass();

    // Variable para definir de que parte se mostrará el sidedrawer
    drawerLocation: SideDrawerLocation;

    //#endregion variables

    constructor(
        private authService: AuthService,
        private dataService: DataService,
        private page: Page,
        private routerExtensions: RouterExtensions
    ) {
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
        this.userActive = ApplicationSettings.getNumber("userActive");
        this.page.actionBarHidden = false;
        this.getDataClient();
    }

    onOpenDrawerTap() {
        if (this.isOpenSidedrawer === false) {
            this.isOpenSidedrawer = true;
            this.drawerComponent.sideDrawer.showDrawer();
        } else if (this.isOpenSidedrawer) {
            this.closeSideDrawer();
        }
    }

    ngOnInit(): void {
        const privacitiy = ApplicationSettings.getBoolean("privacitiy");
        console.log("privacity status => ", privacitiy);
        if (privacitiy === false) {
            this.activatePrivacity = true;
        }
        this.drawerLocation = SideDrawerLocation.Right;
    }

    logOut() {
        this.closeSideDrawer();
        const options = {
            title: "Cerrar sesión",
            message: "¿Desea cerrar la sesión actual?",
            okButtonText: "Sí",
            cancelButtonText: "No"
        };
        confirm(options).then((result: boolean) => {
            if (result) {
                this.isLoading = true;
                this.textLoader = "Cerrando Sesión";
                setTimeout(() => {
                    this.authService.logOut();
                    this.isLoading = false;
                }, 1000);
            }
        });
    }

    receiveStatus(evt: boolean) {
        console.log("Evento ", evt);
        this.activatePrivacity = !evt;
        this.isLoading = true;
        this.textLoader = "Espere un momento, está cargando su información";
        setTimeout(() => {
            this.isLoading = false;
        }, 5000);
    }

    onMenuItemTap(url: string) {
        this.routerExtensions.navigate([url], {
            transition: {
                name: "fade"
            }
        });
        this.closeSideDrawer();
    }

    private getDataClient() {
        const fields: string = `*`;
        const tableName: string = `CLIENTES`;
        const clausule: string = `WHERE ID = ${this.userActive}`;

        let userData: Array<string>;

        this.sqliteMethods.getData(fields, tableName, clausule)
            .then((result: any) => {
                if (result.length > 0) {
                    console.log(`User data length => ${result.length}`);
                    userData = result;
                    this.fullClientName = `${userData[0][1]} ${userData[0][2]} ${userData[0][3]}`;
                } else {
                    console.log(`Carga la informacion desde la api`);
                    this.getDataClientAPI();
                }
            });
    }

    private getDataClientAPI() {
        const dataClient = Array;
        this.dataService.getDataClient(this.userActive)
            // tslint:disable-next-line: deprecation
            .subscribe(
                (data: ClientModel) => {
                    console.log("Data getDataClientApi => ", data);
                    this.fullClientName = `${data.nombre} ${data.paterno} ${data.materno}`;
                    this.dashboardClass.updateDataDashboard(data, this.userActive);
                }
            );
    }

    private closeSideDrawer() {
        this.drawerComponent.sideDrawer.closeDrawer();
        this.isOpenSidedrawer = false;
    }
}
