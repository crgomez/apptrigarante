import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { PoliciesComponent } from "./components/policies/policies.component";
import { DetailsComponent } from "./components/details/details.component";
import { ViewComponent } from "./components/view/view.component";
import { PetitionsComponent } from "./components/petitions/petitions.component";

const routes: Routes = [
    {
        path: "",
        component: PoliciesComponent
    },
    {
        path: "details/:id",
        component: DetailsComponent
    },
    {
        path: "view/:file",
        component: ViewComponent
    },
    {
        path: "petitions",
        component: PetitionsComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PoliciesRoutingModule { }
