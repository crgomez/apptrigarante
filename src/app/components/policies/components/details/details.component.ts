import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "@nativescript/angular";
import { SqliteMethodsClass } from "~/app/core/classes/SqliteMethods";

@Component({
    selector: "app-records",
    templateUrl: "details.component.html",
    styleUrls: ["details.component.scss"]
})

export class DetailsComponent implements OnInit {

    /* detailsArray = [
        { text: "Primer Evento", event: this.firstEvent },
        { text: "Segundo Evento", event: this.secondEvent },
        { text: "Tercer Evento", event: this.thirdEvent }
    ]; */

    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();
    idPolicie: number;
    dataPolicie = [];
    dataCarPolicie: any = [];



    constructor(
        private routerExtensions: RouterExtensions,
        private activateRoute: ActivatedRoute
    ) {
        this.idPolicie = this.activateRoute.snapshot.params.id;
        this.getPoliciesData();
    }

    ngOnInit() { }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    getPoliciesData() {
        const fields: string = `*`;
        const tableName: string = `POLIZAS`;
        const clausule: string = `WHERE ID = ${this.idPolicie}`;


        this.sqliteMethods.getData(fields, tableName, clausule).then((result) => {
            this.dataPolicie = result[0];
            const startDate: string = this.dataPolicie[5];
            this.dataPolicie[5] = startDate.substring(0, 10);
            console.log(this.dataPolicie);
            this.dataCarPolicie = JSON.parse(JSON.stringify(result[0][17]));
            this.dataCarPolicie = JSON.parse(this.dataCarPolicie);
            console.log(this.dataCarPolicie);
        });
    }

    /* firstEvent() {
        console.log("First Event");
    }

    secondEvent() {
        console.log("Second Event");
    }

    thirdEvent() {
        console.log("Third Event");
    } */

}
