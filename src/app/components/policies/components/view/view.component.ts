import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "app-view",
    templateUrl: "view.component.html",
    styleUrls: ["view.component.scss"]
})

export class ViewComponent implements OnInit {

    pdfURL: string;
    constructor(
        private activatedRoute: ActivatedRoute,
        private routerExtensions: RouterExtensions
    ) {
        this.pdfURL = `https://login-app.mark-43.net/auth-cliente/poliza/${this.activatedRoute.snapshot.params.file}`;
    }

    ngOnInit() { }

    onLoad() {
        console.log("Pdf Cargado");
    }

    goBack(){
        this.routerExtensions.backToPreviousPage();
    }
}