import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";

import { PetitionsPoliciesClass } from "../../../../core/classes/PetitionsPoliciesClass";

@Component({
    selector: "app-policies-petitions",
    templateUrl: "petitions.component.html",
    styleUrls: ["petitions.component.scss"]
})

export class PetitionsComponent implements OnInit {

    petitionsData: any[];
    isAccordionOpen: boolean = false;
    idToOpen: number = -1;

    petitionsPoliciesClass: PetitionsPoliciesClass = new PetitionsPoliciesClass();

    constructor(
        private routerExtensions: RouterExtensions
    ) { }

    ngOnInit() {
        this.petitionsData = this.petitionsPoliciesClass.petitionsData;
    }

    actionAccordion(id: number) {
        console.log(this.idToOpen);
        if (this.idToOpen === id) {
            if (this.isAccordionOpen) {
                this.isAccordionOpen = false;
            } else {
                this.isAccordionOpen = true;
            }
        } else {
            this.idToOpen = id;
            this.isAccordionOpen = true;
        }


    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    callCenter() {
        alert("Llamando al callcenter");
    }

    // Instrucciones de pago
    private paymentInstructions() {
        console.log("Instrucciones de pago");
    }

    // Catálogo de facturacion
    private billingCatalgo() {
        console.log("Catálogo de facturación");
    }

    // Identificación oficial
    private officialIdentification() {
        console.log("Identificación oficial");
    }

    // Inspección vehicular
    private vehicleInspection() {
        console.log("Inspección vehicular");
    }

    // Aclaraciones y sugerencias
    private clarificationsAndSuggestions() {
        console.log("Aclaraciones y sugerencias");
    }

    // Cancelar póliza
    private cancelPolicy() {
        console.log("Cancelar póliza");
    }

    // Modificar póliza existente
    private modifyPolicy() {
        console.log("Modificar póliza");
    }

    // En caso de siniestro
    private eventOfClaim() {
        console.log("En caso de siniestro");
    }


}
