import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";
import { ApplicationSettings, Page } from "@nativescript/core";
import { PoliciesClass } from "~/app/core/classes/PoliciesClass";

import { PoliciesService } from "../../../../core/services/policies/policies.service";

@Component({
    selector: "app-policies",
    templateUrl: "policies.component.html",
    styleUrls: ["policies.component.scss"]
})

export class PoliciesComponent implements OnInit {

    userActive: number;
    textLoader: string;

    policiesMethods: PoliciesClass = new PoliciesClass(this.policiesService);

    constructor(
        private routerExtensions: RouterExtensions,
        private policiesService: PoliciesService,
        private page: Page
    ) {
        this.userActive = ApplicationSettings.getNumber("userActive");
        this.textLoader = "Cargando pólizas";
        this.page.actionBarHidden = false;
    }

    ngOnInit() {
        this.getPolicies();
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    getPolicies() {
        this.policiesMethods.getPoliciesSqlite().then(() => {
            this.page.actionBarHidden = false;
        });

    }

    onPetitionsTap() {
        this.routerExtensions.navigate(["policies/petitions"], {
            transition: {
                name: "fade"
            }
        });
    }

    onViewTap(file: string) {
        this.routerExtensions.navigate(["policies/view", file], {
            transition: {
                name: "fade"
            }
        });
    }

    onDetailsTap(id: number) {
        this.routerExtensions.navigate(["policies/details", id], {
            transition: {
                name: "fade"
            }
        });
    }
}
