import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { SharedModule } from "~/app/shared/shared.module";

import { PoliciesComponent } from "./components/policies/policies.component";
import { DetailsComponent } from "./components/details/details.component";
import { PoliciesRoutingModule } from "./policies-routing.module";
import { ViewComponent } from "./components/view/view.component";
import { PetitionsComponent } from "./components/petitions/petitions.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        PoliciesRoutingModule,
        SharedModule
    ],
    exports: [],
    declarations: [
        PoliciesComponent,
        DetailsComponent,
        ViewComponent,
        PetitionsComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PoliciesModule { }
