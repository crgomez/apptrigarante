import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule, NativeScriptFormsModule } from "nativescript-angular";

import { LoginComponent } from "./components/login/login.component";

import { LoginRoutingModule } from "./login-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        LoginRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        NativeScriptFormsModule
    ],
    exports: [],
    declarations: [
        LoginComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class LoginModule { }
