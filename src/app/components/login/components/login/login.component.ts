import { Component, OnInit } from "@angular/core";
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { RouterExtensions } from "nativescript-angular";
import { Page } from "tns-core-modules/ui/page";
import { DataLoginClientModel } from "~/app/core/models/Client/DataLoginClientModel";

import { AuthService } from "../../../../core/services/auth/auth.service";
import { SessionClass } from "../../../../core/classes/SessionClass";
import { ApplicationSettings } from "tns-core-modules";

@Component({
    selector: "app-login",
    templateUrl: "login.component.html",
    styleUrls: ["login.component.scss"]
})

export class LoginComponent implements OnInit {

    actionBarTitle: string = "Iniciar sesión";
    isLogin: boolean = false;
    textLoader: string = "Iniciando Sesión";
    formGroup: FormGroup;
    dataLoginClient: DataLoginClientModel;
    // Instancia de la clase que valida la sesión y retorna la url correspondiente
    validateSession: SessionClass = new SessionClass();
    url: string;

    isShowingPassword = false;

    constructor(
        private page: Page,
        private routerExtensions: RouterExtensions,
        private formBuilder: FormBuilder,
        private authService: AuthService
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.startBuildForm();
    }

    logIn(): void {
        this.isLogin = true;
        const alertOptions = {
            title: "Iniciar sesión",
            message: "Usuario y/o contraseña incorrectos.\nVerifique sus datos e intente de nuevo.",
            okButtonText: "Aceptar"
        };

        if (this.formGroup.valid) {
            /* Se envía como parámetro el formulario ya que este tiene los mismos nombres
            de la interfaz y hacen match automaticamente sin necesidad de crear un nuevo array. */
            this.authService.logInClient(this.formGroup.value)
                .subscribe(
                    (data) => {
                        this.dataLoginClient = data;
                        ApplicationSettings.setNumber("userActive", Number(this.dataLoginClient.id));
                        this.url = this.validateSession.sessionValidate(data);
                    },
                    (error) => {
                        // El error se usa para indicar al usuario que sus datos son incorrectos
                        if (error.status === 500) {
                            console.log("Error LoginComponent => ", error.message);
                            alert(alertOptions);
                        }
                        this.isLogin = false;
                        /* this.page.actionBarHidden = false; */
                    },
                    () => {
                        this.isLogin = false;
                        this.redirectTo();
                    }
                );
        }
    }

    // Método para mostrar u ocultar la contraseña
    showPassword() {
        if (this.isShowingPassword) {
            this.isShowingPassword = false;
        } else {
            this.isShowingPassword = true;
        }
    }

    // Método para reedireccionar a la url correspondiente
    private redirectTo(): void {
        this.routerExtensions.navigate([this.url], {
            transition: {
                name: "fade"
            },
            clearHistory: true
        });
    }

    /* Método que construye el formulario de login con Formularios Reactivos de angular, el cual simplifica
    realizar validaciones y obtener los valores de los texfield */
    private startBuildForm(): void {
        this.formGroup = this.formBuilder.group({
            email: ["", [Validators.required, Validators.pattern("^[^@]+@[^@]+\.[a-zA-Z]{2,}$")]],
            contrasenaApp: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(32)]]
        });
    }

    /* Propiedades get para acceder a las propiedades del formGroup de una manera más sencilla
    Estas propiedades para este caso se usan para obtener los errores existentes
    y mostrarlos al usuario en el pantalla */
    get emailField(): AbstractControl {
        return this.formGroup.get("email");
    }

    get passwordField(): AbstractControl {
        return this.formGroup.get("contrasenaApp");
    }

}
