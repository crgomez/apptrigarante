import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { LayoutComponent } from "./components/layout/layout.component";
import { GuardAuthGuard } from "./guards/guard-auth.guard";

const routes: Routes = [
    {
        path: "",
        component: LayoutComponent,
        children: [
            {
                path: "",
                redirectTo: "/dashboard",
                pathMatch: "full"
            },
            {
                path: "dashboard",
                canActivate: [GuardAuthGuard],
                loadChildren: () => import("./components/dashboard/dashboard.module").then((m) => m.DashboardModule)
            },
            {
                path: "profile",
                canActivate: [GuardAuthGuard],
                loadChildren: () => import("./components/profile/profile.module").then((m) => m.ProfileModule)
            },
            {
                path: "petitions",
                canActivate: [GuardAuthGuard],
                loadChildren: () => import("./components/petitions/petitions.module").then((m) => m.PetitionsModule)
            },
            {
                path: "policies",
                canActivate: [GuardAuthGuard],
                loadChildren: () => import("./components/policies/policies.module").then((m) => m.PoliciesModule)
            },
            {
                path: "login",
                loadChildren: () => import("./components/login/login.module").then((m) => m.LoginModule)
            }
        ]
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
