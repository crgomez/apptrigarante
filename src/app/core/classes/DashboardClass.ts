import { ClientModel } from "../models/Client/ClientModel";
import { SqliteMethodsClass } from "./SqliteMethods";

export class DashboardClass {

    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();

    updateDataDashboard(data: ClientModel, userActive: number) {
        const fields: string = ``;
        const tableName: string = `CLIENTES`;
        const clausule: string = `WHERE ID = ${userActive}`;

        const dataClient = [];
        dataClient.push({ name: `NOMBRE`, value: `'${data.nombre}'` });
        dataClient.push({ name: `A_PATERNO`, value: `'${data.paterno}'` });
        dataClient.push({ name: `A_MATERNO`, value: `'${data.materno}'` });
        dataClient.push({ name: `FECHA_NACIMIENTO`, value: `'${data.fechaNacimiento}'` });
        dataClient.push({ name: `CURP`, value: `'${data.curp}'` });
        dataClient.push({ name: `RFC`, value: `'${data.rfc}'` });
        dataClient.push({ name: `TELEFONO_FIJO`, value: `'${data.telefonoFijo}'` });
        dataClient.push({ name: `TELEFONO_MOVIL`, value: `'${data.telefonoMovil}'` });
        dataClient.push({ name: `CORREO`, value: `'${data.correo}'` });
        dataClient.push({ name: `PAIS`, value: `'${data.nombrePaises}'` });
        dataClient.push({ name: `COLONIA`, value: `'${data.colonia}'` });
        dataClient.push({ name: `CP`, value: `'${data.cp}'` });
        dataClient.push({ name: `CALLE`, value: `'${data.calle}'` });
        dataClient.push({ name: `NUM_EXT`, value: `'${data.numExt}'` });
        dataClient.push({ name: `NUM_INT`, value: `'${data.numInt}'` });
        this.sqliteMethods.updateDataClient(tableName, dataClient, clausule)
            .then((result) => {
                console.log("Status => ", result);
            })
            .catch((error) => {
                console.error("Error UpdateData => ", error);
            });

    }
}