const sqlite = require("nativescript-sqlite");

export class ConnectionClass {

    /* Esta clase está construida con base al patrón de diseño SINGLETON con la finalidad de no
    estar creando instancias innecesarias y así reusar la que ya está abierta sin necesidad de
    volver a instanciar una nueva sino solamente retornar la ya existente y evitar estar saturando
    la base de datos de tantas conexiones. */

    /* Este método estático es el que abre la instancia de la clase y reusarla sin necesidad de
    volver a instanciar la clase con new */
    static getClassInstance(): ConnectionClass {
        /* Primero ser verifica si la variable que almacenará la instancia es nula, si es sí, entonces
        creará la instancia y al final retornará la misma; caso contrario si no es nula, lo único que
        hará es retorarnla ya que esta se encuentra abierta. */
        if (ConnectionClass.singletonInstance === null) {
            // Instancia la clase cuando la variable es nula
            ConnectionClass.singletonInstance = new ConnectionClass();
        }

        // Retorna la instancia de la clase
        return ConnectionClass.singletonInstance;
    }

    // A través de esta variable es que se podrá acceder a los métodos publicos de la clase
    private static singletonInstance: ConnectionClass = null;

    // A través de esta variable es que se podrán realizar las operaciones a la base de datos.
    private static connectionInstance = null;
    
    // Nombre de la base de datos
    private databaseName: string = "master_db";
    
    /* El constructor se vuelve privado para evitar instanciar la clase y obligar a usar el método estático
    getClassInstance() */
    private constructor() { }
    
    /* Primero verifica si la variable que almacenará la instancia de la base de datos es nula, si es sí,
    entonces creará una instancia y la retornará, caso contrario, solo retornará la instancia ya que esta
    se encuentra abierta.
    Es un método asíncrono ya que retornaba la isntancia y su valor era nulo y por tal razón ocurría un error
    al intentar ejecutar el método execSQL, ahora primero resulve el método y al final retorna el resultado*/
    async getConnectionInstance(): Promise<any> {
        if (ConnectionClass.connectionInstance === null) {
            // Espera que tener una respuesta y poder continuar ejecutando el código
            await new sqlite(this.databaseName)
                .then((db: any) => {
                    // Se almacena la instancia de la db en la variable que se reutilizará
                    ConnectionClass.connectionInstance = db;
                });
        }

        // Retorna la variable con la cual se tendrá acceso a la base de datos
        return ConnectionClass.connectionInstance;

    }

    // Método para cerrar la instancia de la clase
    closeClassInstance() {
        ConnectionClass.singletonInstance = null;
    }

    // Método para cerrar la conexión a la base de datos y volver nula la instancia
    closeConnectionInstance() {
        ConnectionClass.connectionInstance.close();
        ConnectionClass.connectionInstance = null;
    }

}
