import { DataLoginClientModel } from "../models/Client/DataLoginClientModel";
import { SqliteMethodsClass } from "./SqliteMethods";
import { SqliteTables } from "./Tables";

export class SessionClass {

    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();
    sqliteTables: SqliteTables = new SqliteTables();

    sessionValidate(data: DataLoginClientModel): string {
        if (data.id > 0) {
            const fields = `ID_CLIENTE`;
            const tableName = `USUARIOS`;
            const clausule = `WHERE ID_CLIENTE = ${data.id}`;

            this.sqliteMethods.getData(fields, tableName, clausule)
                .then(() => {
                    this.updateDataClient(data);
                })
                .catch((error: any) => {
                    console.log(`Error session Validate => ${error.message}`);
                });

            return "/dashboard";
        }

        return "/login";
    }

    updateDataClient(data: DataLoginClientModel) {
        console.log(`Update Cliente SessionClass`);
        const dataArray = [{ name: "ACCESO", value: String(data.tipo) }];
        const tablesName = this.sqliteTables.tablesName;
        const tableName = `USUARIOS`;
        const cluasule = `WHERE ID_CLIENTE ${data.id}`;

        this.sqliteMethods.updateDataClient(tableName, dataArray, cluasule)
            .then((result: any) => {
                if (!result) {
                    // tslint:disable-next-line: prefer-for-of
                    for (let index = 0; index < tablesName.length; index++) {
                        this.sqliteMethods.deleteDataClient(tablesName[index])
                            .then((resultDelete: any) => {
                                console.log(resultDelete);
                                if (index === (tablesName.length - 1)) {
                                    let dataUserArray = [{ name: "ID", value: String(data.id) }];
                                    this.sqliteMethods.setDataClient("CLIENTES", dataUserArray)
                                        .then(() => {
                                            dataUserArray = [
                                                { name: "ID_CLIENTE", value: String(data.id) },
                                                { name: "ACCESO", value: String(data.tipo) }
                                            ];
                                            this.sqliteMethods.setDataClient("USUARIOS", dataUserArray);
                                        });
                                }
                            }).catch((error) => {
                                console.log("\nError en delete tables => ", error.message);
                            });
                    }
                }
            })
            .catch((error: any) => {
                console.log(`Error update data client SessionClass => `, error.message);
            });
    }

}
