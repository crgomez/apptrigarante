export class SqliteTables {
    /* Array que contiene el script de las tablas a crear en la base de datos sqlite
    aquí se agregarán las nuevas tablas a crear */

    tables: Array<string> = [
        `CREATE TABLE IF NOT EXISTS USUARIOS (
            ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            ID_CLIENTE INTEGER NOT NULL,
            NOMBRE TEXT(50) NULL,
            POLIZA_URL TEXT NULL,
            ID_DEVICE TEXT NULL,
            TOKEN TEXT NULL,
            AVISO_PRIVACIDAD TINYINT DEFAULT 0 NOT NULL,
            ACCESO TINYINT DEFAULT 0 NOT NULL,
            CREATED_AT TEXT DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY(ID_CLIENTE) REFERENCES CLIENTES(ID)
        );`,
        `CREATE TABLE IF NOT EXISTS CLIENTES (
            ID INTEGER PRIMARY KEY NOT NULL,
            NOMBRE TEXT(50) NULL,
            A_PATERNO TEXT(50) NULL,
            A_MATERNO TEXT(50) NULL,
            FECHA_NACIMIENTO TEXT NULL,
            CURP TEXT NULL,
            RFC TEXT NULL,
            TELEFONO_FIJO TEXT(15) NULL,
            TELEFONO_MOVIL TEXT(15) NULL,
            CORREO TEXT(50) NULL,
            PAIS TEXT(30) NULL,
            COLONIA TEXT(70) NULL,
            CP TEXT(5) NULL,
            CALLE TEXT(50) NULL,
            NUM_EXT TEXT(30) NULL,
            NUM_INT TEXT(30) NULL,
            CREATED_AT TEXT DEFAULT CURRENT_TIMESTAMP
        );`,
        `CREATE TABLE IF NOT EXISTS POLIZAS (
            ID INTEGER PRIMARY KEY NOT NULL,
            ID_CLIENTE INTEGER NULL,
            POLIZA TEXT(50) NULL,
            FECHA_INICIO TEXT(20) NULL,
            PRIMA_NETA TEXT(20) NULL,
            FECHA_REGISTRO TEXT(20) NULL,
            ARCHIVO TEXT NULL,
            CANTIDAD_PAGOS TEXT NULL,
            DATOS_PAGOS TEXT NULL,
            NOMBRE_COMERCIAL TEXT(50) NULL,
            NUMERO_PAGOS TEXT(10) NULL,
            NUMERO_SERIE TEXT(70) NULL,
            ESTADO_POLIZA TEXT(30) NULL,
            FECHA_PROMESA TEXT(15) NULL,
            ARCHIVO_SUBIDO TEXT(30) NULL,
            TIPO_SUBRAMO TEXT(30) NULL,
            ESTADO_PAGO TEXT NULL,
            DATOS TEXT NULL,
            TIPO_PAGO TEXT(50) NULL,
            PRODUCTO_SOCIO TEXT(50) NULL,
            CREATED_AT TEXT DEFAULT CURRENT_TIMESTAMP
        );`,
        `CREATE TABLE IF NOT EXISTS SOLICITUDES (
            ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            ID_POLIZA INTEGER NOT NULL,
            TIPO_ENDOSO TEXT(20) NOT NULL,
            MOTIVO_ENDOSO TEXT(20) NOT NULL,
            DESCRIPCION TEXT(50) NOT NULL,
            CREATED_AT TEXT DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY(ID_POLIZA) REFERENCES POLIZAS(ID)
        );`
    ];

    tablesName: Array<string> = [
        `CLIENTES`,
        `USUARIOS`,
        `POLIZAS`,
        `SOLICITUDES`
    ];

}
