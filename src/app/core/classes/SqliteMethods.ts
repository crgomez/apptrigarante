import { ConnectionClass } from "./ConnectionClass";
import { SqliteTables } from "./Tables";

export class SqliteMethodsClass {

    constructor() { }
    /* Método para obtener todos los registros de una tabla en específico
    Recibe como parámetros los campos a consultar, el nombre de la tabla y la clausula
    en caso de que se quiera traer registros específicos*/
    async getData(fields: string = "*", tableName: string, clausule: string) {
        // Se obtiene la instancia de la base de datos
        const dbInstance = await ConnectionClass.getClassInstance().getConnectionInstance();
        let rowsArray = [];
        const query: string = `SELECT ${fields} FROM ${tableName} ${clausule}`;
        console.log(`Query => `, query);
        // Método que ejecuta el query directamente a la base de datos y retorna el resultado
        await dbInstance.all(query).then((rows: any) => {
            // Se verifica que la variable rows tenga resultados y se asigna al arrglo local del método
            console.log(rows.length);
            if (rows.length > 0) {
                rowsArray = rows;
            }
        });

        // Se retorna el arreglo ya sea con datos o sin ellos dependiendo del resultado del query
        return rowsArray;
    }

    /* Método para actualizar los datos del cliente, recibe nombre de la tabla, un arreglo de datos
    y de igual manera una clausula en caso de que se quiera actualizar un registro específico*/
    async updateDataClient(tableName: string, data: Array<{ name: string, value: string }>, clausule: string) {
        const dbInstance = await ConnectionClass.getClassInstance().getConnectionInstance();
        // Variabla donde se creará un nuevo arreglo y se asignarán los datos para enviar a la db
        let newData = "";
        let i = 0;

        let statusResult: boolean;

        // Se concatenan el nombre del campo con el valor nuevo que va a tener y se le asigna a la variable newData
        for (const item of data) {
            i++;
            newData += `${item.name} = ${item.value}`;
            if (i < data.length) {
                newData = `${newData},`;
            }
        }
        const query = `UPDATE ${tableName} SET ${newData} ${clausule}`;
        console.log(`Query Update => ${query}`);
        // Método que ejecuta el query directo a la base de datos
        await dbInstance.execSQL(query).then((result: any) => {
            statusResult = true;
        }, () => {
            statusResult = false;
        });

        // Retorna el estatus dependiendo de la respuesta que arrojó el query de la base de datos
        return statusResult;
    }

    async deleteDataClient(tableName: string, clausule: string = "") {
        let statusDelete: boolean;
        const dbInstance = await ConnectionClass.getClassInstance().getConnectionInstance();
        const query = `DELETE FROM ${tableName} ${clausule}`;

        await dbInstance.execSQL(query).then(() => {
            statusDelete = true;
        }, () => {
            statusDelete = false;
        });

        return statusDelete;
    }

    async setDataClient(tableName: string, data: Array<{ name: string, value: string }>) {
        const dbInstance = await ConnectionClass.getClassInstance().getConnectionInstance();
        let statusInsert: boolean;
        let cols = "";
        let privateValues = "";
        const values: Array<string> = [];
        let i = 0;
        for (const item of data) {
            i++;
            cols += item.name;
            values.push(item.value);
            privateValues += "?";
            if (i < data.length) {
                cols = `${cols},`;
                privateValues = `${privateValues},`;
            }
        }

        const query = `INSERT INTO ${tableName}(${cols}) VALUES(${privateValues})`;

        await dbInstance.execSQL(query, values).then((result: any) => {
            statusInsert = true;
        }, () => {
            statusInsert = false;
        });

        return statusInsert;
    }

    async createTablesSqlite() {
        const sqliteTables: SqliteTables = new SqliteTables();
        // Se obtiene la instancia de la conexión para usarla sin necesidad de estar creando varias a través de new
        const dbInstance = await ConnectionClass.getClassInstance().getConnectionInstance();
        const tables = sqliteTables.tables;
        // Ciclo foreach para ejecutar el script y crear las tablas en vez de anidar los execSQL
        tables.forEach((table: string) => {
            dbInstance.execSQL(table)
                .then(() => {
                    console.log(`Create table result => Success created`);
                }, (error: any) => {
                    console.log(`Create table error => ${error}`);
                });
        });
        ConnectionClass.getClassInstance().closeConnectionInstance();
        ConnectionClass.getClassInstance().closeClassInstance();
    }

}
