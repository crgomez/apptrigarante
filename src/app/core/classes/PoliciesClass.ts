import { ApplicationSettings } from "@nativescript/core";
import { PolicieModel } from "../models/Policie/PolicieModel";
import { PoliciesService } from "../services/policies/policies.service";
import { SqliteMethodsClass } from "./SqliteMethods";

export class PoliciesClass {

    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();
    userActive: number;

    dataLoaded: boolean = false;

    idPolicie: any;

    iconsArray: Array<{ name: string, url: string }>;
    policiesArray: Array<{
        id: number,
        poliza: string,
        tipoSubramo: string,
        cantidadPagos: string,
        productoSocio: string,
        primaNeta: string,
        nombreComercial: string,
        archivo: string,
        urlImage: string;
    }> = [];

    constructor(
        private policiesService: PoliciesService
    ) {
        this.userActive = ApplicationSettings.getNumber("userActive");
        // this.getPoliciesSqlite();
    }

    async getPoliciesSqlite() {

        const fields: string = `ID`;
        const tableName: string = `POLIZAS`;
        const clausule: string = `WHERE ID_CLIENTE = ${this.userActive}`;

        await this.sqliteMethods.getData(fields, tableName, clausule)
            .then((result) => {
                console.log("Result.length", result.length);
                if (result.length > 0) {
                    this.idPolicie = result[0];
                    this.getClientPoliciesData();
                    this.getPoliciesAPI();
                    this.dataLoaded = true;
                } else {
                    console.log("Ir al API");
                    this.getPoliciesAPI();

                }
            }).catch(() => {
                this.getPoliciesAPI();
            });

    }

    private async getPoliciesAPI() {
        let policiesData: Array<PolicieModel>;
        this.policiesService.getRegisterClient(this.userActive)
            // tslint:disable-next-line: deprecation
            .subscribe((data) => {
                policiesData = data;
                console.log("DataAPI => ", data);
            }, (error) => {
                console.log(error.message);
                this.dataLoaded = false;
            }, () => {
                this.setDataSqlite(policiesData);
                // this.dataLoaded = true;
            });
    }

    private async setDataSqlite(policiesData: Array<PolicieModel>) {
        console.log("Entro en setDataSqlite");
        const newPoliciesData = [];
        for (const policie of policiesData) {
            newPoliciesData.push({ name: "ID", value: policie.id });
            newPoliciesData.push({ name: "ID_CLIENTE", value: this.userActive });
            newPoliciesData.push({ name: "POLIZA", value: policie.poliza });
            newPoliciesData.push({ name: "FECHA_INICIO", value: policie.fechaInicio });
            newPoliciesData.push({ name: "PRIMA_NETA", value: policie.primaNeta });
            newPoliciesData.push({ name: "FECHA_REGISTRO", value: policie.fechaRegistro });
            newPoliciesData.push({ name: "ARCHIVO", value: policie.archivo });
            newPoliciesData.push({ name: "CANTIDAD_PAGOS", value: policie.cantidadPagos });
            newPoliciesData.push({ name: "DATOS_PAGOS", value: policie.datosPagos });
            newPoliciesData.push({ name: "NOMBRE_COMERCIAL", value: policie.alias });
            newPoliciesData.push({ name: "NUMERO_PAGOS", value: policie.numeroPagos });
            newPoliciesData.push({ name: "NUMERO_SERIE", value: policie.numeroSerie });
            newPoliciesData.push({ name: "ESTADO_POLIZA", value: policie.estadoPoliza });
            newPoliciesData.push({ name: "FECHA_PROMESA", value: policie.fechaPromesaPago });
            newPoliciesData.push({ name: "ARCHIVO_SUBIDO", value: policie.archivoSubido });
            newPoliciesData.push({ name: "TIPO_SUBRAMO", value: policie.tipoSubramo });
            newPoliciesData.push({ name: "ESTADO_PAGO", value: policie.estadoPago });
            newPoliciesData.push({ name: "DATOS", value: JSON.stringify(JSON.parse(policie.datos)) });
            newPoliciesData.push({ name: "TIPO_PAGO", value: policie.tipoPago });
            newPoliciesData.push({ name: "PRODUCTO_SOCIO", value: policie.productoSocio });
            await this.sqliteMethods.deleteDataClient("POLIZAS", "").then((resultDelete) => {
                console.log("ResultDelete1 => ", resultDelete);
                if (resultDelete) {
                    this.sqliteMethods.setDataClient("POLIZAS", newPoliciesData).then((resultSet) => {
                        if (resultSet) {
                            console.log("ResultSet1 => ", resultSet);
                            this.getClientPoliciesData();
                            // this.dataLoaded = true;
                        }
                    }).catch((errorSet) => {
                        console.log("ErrorSet1 => ", errorSet);
                        this.dataLoaded = false;
                    });
                }
            }).catch((errorDelete) => {
                console.log("resultDelete => ", errorDelete);
                this.sqliteMethods.setDataClient("POLIZAS", newPoliciesData)
                    .then((resultSet) => {
                        console.log("ResultSet2 => ", resultSet);
                        this.getClientPoliciesData();
                        // this.dataLoaded = true;
                    }).catch((errorSet) => {
                        console.log("ErrorSet2 => ", errorSet);
                        this.dataLoaded = false;
                    });
            });
        }
    }

    private async getClientPoliciesData() {

        const fieldsGet: string = `ID`;
        const tableNameGet: string = `POLIZAS`;
        const clausuleGet: string = `WHERE ID_CLIENTE = ${this.userActive}`;
        await this.sqliteMethods.getData(fieldsGet, tableNameGet, clausuleGet).then((result) => {
            console.log("ResultGetClient.length", result.length);
            if (result.length > 0) {
                this.idPolicie = result[0];
                this.policiesArray = [];
                const fields: string = `ID, POLIZA, TIPO_SUBRAMO, CANTIDAD_PAGOS, PRODUCTO_SOCIO, PRIMA_NETA, NOMBRE_COMERCIAL, ARCHIVO`;
                const tableName: string = `POLIZAS`;
                // tslint:disable-next-line: prefer-for-of
                for (let index = 0; index < this.idPolicie.length; index++) {
                    const clausule: string = `WHERE ID = ${this.idPolicie[index]}`;
                    this.sqliteMethods.getData(fields, tableName, clausule).then((resultget) => {
                        console.log("Resulta =>", resultget);
                        const url = this.getUrlImage(resultget[index][6]);
                        this.policiesArray.push({
                            id: Number(resultget[index][0]),
                            poliza: resultget[index][1],
                            tipoSubramo: resultget[index][2],
                            cantidadPagos: resultget[index][3],
                            productoSocio: resultget[index][4],
                            primaNeta: resultget[index][5],
                            nombreComercial: resultget[index][6],
                            archivo: resultget[index][7],
                            urlImage: url
                        });
                        /* const policiesData = result; */
                    });
                }
                this.dataLoaded = true;
            }

        }).catch((error) => {
            console.log("Error en getClientePolicesData => ", error);
        });

        console.log("idPolicies => ", this.idPolicie);
        console.log(this.idPolicie.length);

        /* if (this.policiesArray.length > 0) {
            this.dataLoaded = true;
        } */
        console.log(this.policiesArray);
    }

    private getUrlImage(id: string) {
        const url: string = `~/assets/images/iconos-aseguradoras`;
        const images = [
            { id: "ABA", name: "aba.png" },
            { id: "SEGUROS AFIRME", name: "afirme.png" },
            { id: "ANA SEGUROS", name: "ana.png" },
            { id: "AXA", name: "axa.png" },
            { id: "BANORTE", name: "banorte.png" },
            { id: "GENERAL DE SEGUROS", name: "general.png" },
            { id: "GNP", name: "gnp.png" },
            { id: "HDI", name: "hdi.png" },
            { id: "INBURSA", name: "inbursa.png" },
            { id: "MAPFRE", name: "mapfre.png" },
            { id: "MIGO", name: "migo.jpg" },
            { id: "EL POTOSI", name: "elpotosi.png" },
            { id: "QUALITAS", name: "qualitas.png" },
            { id: "ZURA", name: "sura.png" },
            { id: "ZURICH", name: "zurich.png" },
            { id: "EL AGUILA", name: "elaguila.png" },
            { id: "AIG", name: "aig.png" },
            { id: "LA LATINO", name: "lalatino.png" }
        ];

        return `${url}/${images.find((e) => e.id === id).name}`;

    }

}
