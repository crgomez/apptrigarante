export class PetitionsPoliciesClass {
    petitionsData = [
        {
            id: 0,
            title: "Instrucciones de pago",
            subtitle: "",
            text: "",
            phone: "",
            icon: "",
            event: this.paymentInstructions
        },
        {
            id: 1,
            title: "Catálogo de facturación",
            subtitle: "",
            text: "",
            phone: "",
            icon: "",
            event: this.billingCatalgo
        },
        {
            id: 2,
            title: "Identificación oficial",
            subtitle: "¿Cuáles son los documentos válidos?",
            text: `
            - INE (Vigente)
            - Pasaporte (Vigente)
            - Cédula Profesional`,
            phone: "",
            icon: "",
            event: this.officialIdentification
        },
        {
            id: 3,
            title: "Inspección vehicular",
            subtitle: "Línea de atención a clientes",
            text: "Marque la opción 6",
            phone: "5536024275",
            icon: String.fromCharCode(0xf095),
            event: this.vehicleInspection
        },
        {
            id: 4,
            title: "Aclaraciones y sugerencias",
            subtitle: "Comuníquese a nuestra línea",
            text: "Para cualquier aclaración o sugerencia relacionada con su póliza",
            phone: "5588902464",
            icon: String.fromCharCode(0xf095),
            event: this.clarificationsAndSuggestions
        },
        {
            id: 5,
            title: "Cancelar póliza",
            subtitle: "Comuníquese a nuestra linea",
            text: "Opción 5",
            phone: "5536024275",
            icon: String.fromCharCode(0xf095),
            event: this.cancelPolicy
        },
        {
            id: 6,
            title: "Modificar póliza existente",
            subtitle: "",
            text: "",
            phone: "",
            icon: "",
            event: this.modifyPolicy
        },
        {
            id: 7,
            title: "En caso de siniestro",
            subtitle: "",
            text: "Ver lista",
            phone: "",
            icon: "",
            event: this.eventOfClaim
        }
    ];

    // Instrucciones de pago
    private paymentInstructions() {
        console.log("Instrucciones de pago");
    }

    // Catálogo de facturacion
    private billingCatalgo() {
        console.log("Catálogo de facturación");
    }

    // Identificación oficial
    private officialIdentification() {
        console.log("Identificación oficial");
    }

    // Inspección vehicular
    private vehicleInspection() {
        console.log("Inspección vehicular");
    }

    // Aclaraciones y sugerencias
    private clarificationsAndSuggestions() {
        console.log("Aclaraciones y sugerencias");
    }

    // Cancelar póliza
    private cancelPolicy() {
        console.log("Cancelar póliza");
    }

    // Modificar póliza existente
    private modifyPolicy() {
        console.log("Modificar póliza");
    }

    // En caso de siniestro
    private eventOfClaim() {
        console.log("En caso de siniestro");
    }
}