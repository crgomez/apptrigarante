import { NgModule } from "@angular/core";

import { AuthService } from "./services/auth/auth.service";
import { DataService } from "./services/data/data.service";
import { PoliciesService } from "./services/policies/policies.service";

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [
        AuthService,
        DataService,
        PoliciesService
    ]
})
export class CoreModule { }
