import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { RouterExtensions } from "nativescript-angular";
import { Observable } from "rxjs";
import { ApplicationSettings } from "tns-core-modules";
import { environment } from "~/environment/environment";
import { DataLoginClientModel } from "../../models/Client/DataLoginClientModel";
import { LoginModel } from "../../models/Login/LoginModel";

@Injectable({ providedIn: "root" })
export class AuthService {

    private userActive: number;

    constructor(
        private http: HttpClient,
        private routerExtensions: RouterExtensions
    ) { }

    // Realiza la petición a la API enviando como parámetros correo y contraseña y este retorna informacion
    // en caso de que las credenciales coincidan con las que están registradas en la base de datos.
    logInClient(dataLogin: LoginModel): Observable<DataLoginClientModel> {
        return this.http.post<DataLoginClientModel>(`${environment.apiNode}/login`, dataLogin);
    }

    // Método para eliminar al usuario de la configuración de la app y retornarlo a la pantalla del logueo
    logOut() {
        ApplicationSettings.remove("userActive");
        this.routerExtensions.navigate(["/login"], {
            transition: {
                name: "fade"
            },
            clearHistory: true
        });
    }

    // Mpetodo que verifica si ya existe un usuario en la configuracion y permitir el acceso controlado por el guard
    hasUserActive() {
        if (ApplicationSettings.getNumber("userActive")) {
            this.userActive = Number(ApplicationSettings.getNumber("userActive"));
            if (this.userActive > -1) {
                return true;
            } else {
                return false;
            }
        }
    }

}