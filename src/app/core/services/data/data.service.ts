import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "~/environment/environment";
import { ClientModel } from "../../models/Client/ClientModel";

@Injectable({
    providedIn: "root"
})
export class DataService {

    constructor(
        private http: HttpClient
    ) {

    }

    getDataClient(id: number): Observable<ClientModel> {
        return this.http.get<ClientModel>(`${environment.apiMark}/v1/cliente/${id}`);
    }
}
