import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "~/environment/environment";
import { PolicieModel } from "../../models/Policie/PolicieModel";

@Injectable({ providedIn: "root" })
export class PoliciesService {

    constructor(
        private http: HttpClient
    ) { }

    getRegisterClient(idClient: number): Observable<Array<PolicieModel>> {
        return this.http.get<Array<PolicieModel>>(`${environment.apiMark}/v1/app-cliente/get-registro-cliente/${idClient}`);
    }

}