import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from "@angular/router";
import { RouterExtensions } from "nativescript-angular";
import { Observable } from "rxjs";
import { ApplicationSettings } from "tns-core-modules";
import { AuthService } from "../core/services/auth/auth.service";

@Injectable({
    providedIn: "root"
})
export class GuardAuthGuard implements CanActivate {

    constructor(
        private authService: AuthService,
        private routerExtensions: RouterExtensions
    ) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (this.authService.hasUserActive()) {
            // Se valida que exista la vaiable en la configuracion de la aplicación
            if (ApplicationSettings.getBoolean("privacitiy")) {
                // Si la variable existe mostrará este mensaje en consola
            } else {
                // Si la variable  no existe se creará y tendrá como valor false
                ApplicationSettings.setBoolean("privacitiy", false);
            }

            return true;
        } else {
            this.routerExtensions.navigate(["/login"], {
                transition: {
                    name: "fade"
                },
                clearHistory: true
            });

            return false;
        }
    }
}
